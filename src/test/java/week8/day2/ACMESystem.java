package week8.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;


public class ACMESystem {
	
	@Test
	public static void practice() throws InterruptedException {
		 System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		 ChromeDriver driver= new ChromeDriver();
		 driver.get("https://acme-test.uipath.com/account/login");
		 driver.manage().window().maximize();
		 driver.findElementById("email").sendKeys("swathiselvam11@gmail.com");
		 driver.findElementById("password").sendKeys("Vani@051");
		 driver.findElementById("buttonLogin").click();
		 Thread.sleep(2000);
		 Actions builder=new Actions(driver);
		 WebElement eleVendor = driver.findElementByXPath("(//button[@type='button'])[6]");
		 builder.moveToElement(eleVendor).build().perform();
		 Thread.sleep(2000);
		 driver.findElementByLinkText("Search for Vendor").click();
		 driver.findElementById("vendorTaxID").sendKeys("RO212121");
		 driver.findElementById("buttonSearch").click();
		 String vendor = driver.findElementByXPath("//table[@class='table']//td").getText();
		 System.out.println(vendor);
	}
		
	}
