package steps;

import com.yalla.selenium.api.base.SeleniumBase;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends SeleniumBase{
	
	@Before
	public void beforeScenario(Scenario sc) {
/*		System.out.println(sc.getName());
		System.out.println(sc.getId());*/
		
		//@BeforeSuite in reporter in POM- utils
		startReport();
		//@BeforeClass in reporter in POM- utils
		test = extent.createTest(sc.getName(), sc.getId());
	    test.assignAuthor("Swathi");
	    test.assignCategory("Smoke"); 
	    //@BeforeMethod in Annotations
	    startApp("chrome", "http://leaftaps.com/opentaps");
	}
	@After
	public void afterScenario(Scenario sc) {
		/*System.out.println(sc.getStatus());*/
		
		//@AfterMethod in Annotation
		close();
		//@AfterSuite in Reporter in POM - utils
		stopReport();
	}
	
}
