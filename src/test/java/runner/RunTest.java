package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="src\\test\\java\\feature\\CreateLead.feature",
		glue= {"com\\yalla\\pages","steps"},
		monochrome= true
/*		dryRun= false,
		snippets= SnippetType.CAMELCASE, tags= "@regression or @smoke" */
		//tags= "@regression"
		)
public class RunTest {

}
