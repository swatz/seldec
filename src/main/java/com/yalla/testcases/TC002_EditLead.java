package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC002_EditLead";
		testcaseDec = "Edit a Lead";
		author = "Swathi";
		category = "smoke";
		excelFileName = "TC002";
	} 
	
	

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd,String fName, String UpdatedLastName) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.ClickLeads()
		.ClickFindLeads()
		.enterFName(fName)
		.clickFindLeadsButton()
		.clickLeadID()
		.clickEdit()
		.enterLName(UpdatedLastName)
		.clickUpdate()
		.VerifyUpdated(UpdatedLastName)
		.logOut();
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}






