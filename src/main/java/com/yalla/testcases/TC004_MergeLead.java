package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_MergeLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC004_MergeLead";
		testcaseDec = "Merge two Leads";
		author = "Swathi";
		category = "smoke";
		excelFileName = "TC004";
	} 
	
	

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd, String fromfName, String toLead, String MergeNoData) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.ClickLeads()
		.ClickMergeLeads()
		.ClickFromLeadImg()
		.enterFName(fromfName)
		.clickFindLeadsButton()
		.clickLeadIDMerge1()
		.ClickToLeadImg()
		.enterLeadID(toLead)
		.clickFindLeadsButton()
		.clickLeadIDMerge2()
		.clickMerge()
		.ClickFindLeadsMerge()
		.enterFName(fromfName)
		.clickFindLeadsButton()
		.VerifyMergedLead(MergeNoData);
	}
	
}






