package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC005_DuplicateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC005_DuplicateLead";
		testcaseDec = "duplicate a Lead";
		author = "Swathi";
		category = "smoke";
		excelFileName = "TC005";
	} 
	
	

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd,String phone) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.ClickLeads()
		.ClickFindLeads()
		.clickPhone()
		.enterPhoneNumber(phone)
		.clickFindLeadsButton()
		.clickLeadID()
		.ClickDuplicateLead()
		.ClickCreateLeads()
		.ClickFindLeadsMerge()
		.clickPhone()
		.enterPhoneNumber(phone)
		.clickFindLeadsButton();
	}
	
}






