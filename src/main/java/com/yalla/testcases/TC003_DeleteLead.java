package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_DeleteLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_DeleteLead";
		testcaseDec = "delete a Lead";
		author = "Swathi";
		category = "smoke";
		excelFileName = "TC003";
	} 
	
	

	@Test(dataProvider="fetchData") 
	public void EditLead(String uName, String pwd, String LeadID, String NoData) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.ClickLeads()
		.ClickFindLeads()
		.enterLeadID(LeadID)
		.clickFindLeadsButton()
		.clickLeadID()
		.clickDelete()
		.ClickFindLeads()
		.enterLeadID(LeadID)
		.clickFindLeadsButton()
		.VerifyDeletedLead(NoData);
	}
	
}






