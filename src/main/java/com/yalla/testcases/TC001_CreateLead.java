package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC001_CreateLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC001_CreateLead";
		testcaseDec = "create Lead";
		author = "Swathi Selvaraj";
		category = "smoke";
		excelFileName = "TC001";
	} 
	
	

	@Test(dataProvider="fetchData") 
	public void CreateLead(String uName, String pwd,String CompName, String fName, String lName  ) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.ClickLeads()
		.ClickCreateLeads()
		.enterCompanyName(CompName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLeadButton()
		.VerifyFirstName(fName)
		.logOut();
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	
}






