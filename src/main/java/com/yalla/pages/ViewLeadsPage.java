package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class ViewLeadsPage extends Annotations{
	
	public ViewLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//span[@id='viewLead_firstName_sp']") WebElement eleVerifyFirstName;
	@FindBy(how=How.LINK_TEXT, using="Logout") WebElement eleLogout;
	@FindBy(how=How.XPATH, using="//a[text()='Edit']") WebElement eleEdit;
	@FindBy(how=How.XPATH, using="//a[text()='Delete']") WebElement eleDelete;
	@FindBy(how=How.ID, using="viewLead_lastName_sp") WebElement eleVerifyUpdatedLastName;
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement elefindLeads;
	@FindBy(how=How.LINK_TEXT, using="Duplicate Lead") WebElement eleDuplicateLead;
	
	@And("Verify first name")
	public ViewLeadsPage VerifyFirstName(String firstNameEntered) {
		String text=eleVerifyFirstName.getText();
		if(text.equals(firstNameEntered)) {
			System.out.println("created Lead Successfully");
		}
		else {
			System.err.println("Test case failed");
		}
		return this;
	}
	
	public ViewLeadsPage logOut() {
		click(eleLogout);
		System.out.println("Logged out successfully");
		return this;
	}
	
	public OpenTapsCRM clickEdit() {
		click(eleEdit);
		return new OpenTapsCRM();
	}
	
	public ViewLeadsPage VerifyUpdated(String LNameUpdate) {
		String text2=eleVerifyUpdatedLastName.getText();
		if(text2.equals(LNameUpdate)) {
			System.out.println("edited Lead Successfully");
		}
		else {
			System.err.println("Test case failed");
		}
		return this;
		
	}
	
	public MyLeadsPage clickDelete() {
		click(eleDelete);
		return new MyLeadsPage();
	}
	
	public FindLeadsPage ClickFindLeadsMerge() {
		click(elefindLeads);
		return new FindLeadsPage();
	}
	
	public DuplicateLeadPage ClickDuplicateLead() {
		click(eleDuplicateLead);
		return new DuplicateLeadPage();
	}
	
	
}
