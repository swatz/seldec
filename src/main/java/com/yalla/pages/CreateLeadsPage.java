package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class CreateLeadsPage extends Annotations{
	
	public CreateLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName")  WebElement eleCompanyName;
	@FindBy(how=How.ID, using="createLeadForm_firstName")  WebElement eleFirstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName")  WebElement eleLastName;
	@FindBy(how=How.XPATH, using="//input[@class='smallSubmit']") WebElement eleCreateLeadButton;
	
	@And("Enter the Company Name")
	public CreateLeadsPage enterCompanyName(String data) {
		clearAndType(eleCompanyName, data);  
		return this; 
	}
	
	@And("Enter the First Name")
	public CreateLeadsPage enterFirstName(String data) {
		clearAndType(eleFirstName, data); 
		return this; 
	}
	
	@And("Enter the Last Name")
	public CreateLeadsPage enterLastName(String data) {
		clearAndType(eleLastName, data); 
		return this; 
	}
	
	@And("Click Create Lead Button")
	public ViewLeadsPage clickCreateLeadButton() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
          click(eleCreateLeadButton);  
          return new ViewLeadsPage();
	}
	
	
	
	
}
