package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;

public class MyLeadsPage extends Annotations{
	
	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using="Create Lead") WebElement eleCreateLeads;
	@FindBy(how=How.LINK_TEXT, using="Find Leads") WebElement elefindLeads;
	@FindBy(how=How.LINK_TEXT, using="Merge Leads") WebElement eleMergeLeads;
	
	@And("Click on Create Leads Button")
	public CreateLeadsPage ClickCreateLeads() {
		click(eleCreateLeads);
		return new CreateLeadsPage();
	}
	
	
	public FindLeadsPage ClickFindLeads() {
		click(elefindLeads);
		return new FindLeadsPage();
	}
	
	public MergeLeadsPage ClickMergeLeads() {
		click(eleMergeLeads);
		return new MergeLeadsPage();
	}
	
}
