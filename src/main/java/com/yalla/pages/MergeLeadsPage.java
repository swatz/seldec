package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadsPage extends Annotations{
	
	public MergeLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.XPATH, using="//img[@alt='Lookup']") WebElement eleFromLeadImg;
	@FindBy(how=How.XPATH, using="(//img[@alt='Lookup'])[2]") WebElement eleToLeadImg;
	@FindBy(how=How.LINK_TEXT, using="Merge") WebElement eleMerge;
	
	
	public FindLeadsPage ClickFromLeadImg() {
		click(eleFromLeadImg);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	public FindLeadsPage ClickToLeadImg() {
		click(eleToLeadImg);
		switchToWindow(1);
		return new FindLeadsPage();
	}
	
	public ViewLeadsPage clickMerge() {
		click(eleMerge);
		acceptAlert();
		return new ViewLeadsPage();
	}
	
}
