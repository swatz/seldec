package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations{ 

	public FindLeadsPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="(//div[@class='x-form-item x-tab-item']//input)[2]") WebElement elefName;
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how=How.XPATH, using="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId'])/a") WebElement eleLeadID;
	@FindBy(how=How.XPATH, using="(//div[@class='x-form-item x-tab-item']//input)[1]") WebElement elementLeadID;
	@FindBy(how=How.XPATH, using="//div[text()='No records to display']") WebElement eleverifyLeadID;
	@FindBy(how=How.XPATH, using="//span[text()='Phone']") WebElement elePhone;
	@FindBy(how=How.XPATH, using="//input[@name='phoneNumber']") WebElement elePhoneNum;
	
	
	public FindLeadsPage enterFName(String data) {
		clearAndType(elefName, data);  
		return this;
	}
	
	
	public FindLeadsPage enterLeadID(String data) {
		clearAndType(elementLeadID, data);  
		return this;
	}
	
	
	public FindLeadsPage clickFindLeadsButton() {
		click(eleFindLeads);
		System.out.println("success");
		return this;	
	}

	public ViewLeadsPage clickLeadID() throws InterruptedException {
		Thread.sleep(2000);
		click(eleLeadID);
		return new ViewLeadsPage();
	}
	
	public MergeLeadsPage clickLeadIDMerge1() throws InterruptedException {
		Thread.sleep(2000);
		click(eleLeadID);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
	
	public MergeLeadsPage clickLeadIDMerge2() throws InterruptedException {
		Thread.sleep(2000);
		click(eleLeadID);
		switchToWindow(0);
		return new MergeLeadsPage();
	}
 
	
	public FindLeadsPage VerifyDeletedLead(String data) {
		String text3=eleverifyLeadID.getText();
		if(text3.equals(data)) {
			System.out.println("deleted Lead Successfully");
		}
		else {
			System.err.println("Test case failed");
		}
		return this;
		
	}
	public FindLeadsPage VerifyMergedLead(String data) {
		String text4=eleverifyLeadID.getText();
		if(text4.equals(data)) {
			System.out.println("Merged Lead Successfully");
		}
		else {
			System.err.println("Test case failed");
		}
		return this;
		
	}
	
	public FindLeadsPage clickPhone() {
		click(elePhone);
		return this;
	}
	
	public FindLeadsPage enterPhoneNumber(String data) {
		clearAndType(elePhoneNum, data);
		return this;
	}

}







