package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class OpenTapsCRM extends Annotations{ 

	public OpenTapsCRM() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.ID, using="updateLeadForm_lastName") WebElement eleLName;
	@FindBy(how=How.XPATH, using="//input[@value='Update']") WebElement eleUpdate;
	
	public OpenTapsCRM enterLName(String data) {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		clearAndType(eleLName, data);  
		return this;	
	}

	public ViewLeadsPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadsPage();
	}
}







